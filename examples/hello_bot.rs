// Copyright (c) 2019 Emmanuel Gil Peyrot <linkmauve@linkmauve.fr>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use futures::{Future, Stream};
use std::env::args;
use std::process::exit;
use std::str::FromStr;
use tokio::runtime::current_thread::Runtime;
use xmpp::{ClientBuilder, ClientFeature, ClientType, Event};
use xmpp_parsers::{message::MessageType, Jid};

fn main() {
    let args: Vec<String> = args().collect();
    if args.len() != 5 {
        println!("Usage: {} <jid> <password> <room JID> <nick>", args[0]);
        exit(1);
    }
    let jid = &args[1];
    let password = &args[2];
    let room_jid = &args[3];
    let nick: &str = &args[4];

    // tokio_core context
    let mut rt = Runtime::new().unwrap();

    // Client instance
    let client = ClientBuilder::new(jid, password)
        .set_client(ClientType::Bot, "xmpp-rs")
        .set_website("https://gitlab.com/xmpp-rs/xmpp-rs")
        .enable_feature(ClientFeature::Avatars)
        .build()
        .unwrap();

    let mut agent = client.get_agent();

    let future = client
        .listen()
        .for_each(|evt: Event| {
            match evt {
                Event::Online => {
                    println!("Online.");
                    let room_jid = Jid::from_str(room_jid).unwrap().with_resource(nick);
                    agent.join_room(room_jid, "en", "Yet another bot!");
                }
                Event::Disconnected => {
                    println!("Disconnected.");
                    //return Err(());
                }
                Event::RoomJoined(jid) => {
                    println!("Joined room {}.", jid);
                    agent.send_message(
                        jid.into_bare_jid(),
                        MessageType::Groupchat,
                        "en",
                        "Hello world!",
                    );
                }
                Event::AvatarRetrieved(jid, path) => {
                    println!("Received avatar for {} in {}.", jid, path);
                }
            }
            Ok(())
        })
        .map_err(|e| println!("{:?}", e));

    rt.block_on(future).unwrap()

    // Start polling
    /*match rt.block_on(client.select2(forwarder).map(|_| ()).map_err(|_| ())) {
        Ok(_) => (),
        Err(e) => {
            println!("Fatal: {:?}", e);
            ()
        }
    }*/
}
